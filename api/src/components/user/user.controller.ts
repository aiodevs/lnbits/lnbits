import { FastifyReply, FastifyRequest } from "fastify";
import { CreateUser } from "./user.schema";
import UserService, { EnrichedUser } from "./user.service";
import SessionService from "@components/auth/session.service";

export default class UserController {
  private userService: UserService = new UserService();
  private sessionService: SessionService= new SessionService();

  public createUser = async (req: FastifyRequest<{ Body: CreateUser }>, reply: FastifyReply) => {
    // TODO consider removing the creation of a session straight away
    const user:EnrichedUser = await this.userService.createUser(req.body);
    const sessionId = user.auth_session[0]?.id;
    
    if (sessionId) {
      this.sessionService.setSessionCookie(reply, sessionId);
    }

    return {
      message: "Signup successful",
      user:{
       email: user.email 
      }
     };
  }
}
