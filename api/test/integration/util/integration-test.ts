import Api from "@/api";
import { connectToDb } from "@/util/prisma";
import { UUID } from "crypto";
import supertest from "supertest";
import { StartedDockerComposeEnvironment } from "testcontainers";
import { TestContext } from "vitest";
import TestUtils from "./test-util";

// TODO speedup:
// - migrate reset without any fancy schema switching
// or
// - parrelize everything including the migrate with a env bind
// TODO move before... to api-integration-setup.ts
// Clone schema
// Set up one container for each test suite
// Create a db reset function that manually deletes all the data using  prisma.deleteMany

export interface ApiIntegrationTest extends TestContext {
  api: Api;
  server: supertest.SuperTest<supertest.Test>;
  schema: UUID;
  beforeTime: number;
  startTime: number;
}

export interface testSuite{
    environment: StartedDockerComposeEnvironment;
    server : supertest.SuperTest<supertest.Test>;
}

export class IntegrationTestFastResetDB {
  public static async beforeAll(port: string): Promise<testSuite> {
    const start = Date.now();

    const props = TestUtils.getEnvProperties();
    props.port = port;
    const url = TestUtils.databaseURL(props);
    const environment = await TestUtils.dbUp(props);

    await TestUtils.initDB(url);

    const api = new Api(url);
    await api.app.ready();
    const server = supertest(api.app.server);

    console.log("beforeAll: ", Date.now() - start);
    return {server, environment} ;
  }

  public static async beforeEach (context: ApiIntegrationTest, {server}: testSuite){
    context.startTime = Date.now();
    context.server = server;

    await TestUtils.resetDBviaPrisma();

    context.beforeTime = Date.now() - context.startTime;
  };

  public static afterEach = async (context: ApiIntegrationTest) => {
    const start = Date.now();

    const after = Date.now() - start;
    const before = context.beforeTime;
    const totalTime = Date.now() - context.startTime;
    const totalTest = totalTime - after - before;
    console.log(
      `Test took: ${totalTest}ms before ${before}ms after ${after}ms total: ${totalTime}ms | name: ${context.task.name} | mode: ${context.task.mode} | concurrent: ${context.task.concurrent}`
    );
  };
}

export class IntegrationTestNewSchemaPerTest {
  public static async beforeAllDbUp(): Promise<StartedDockerComposeEnvironment> {
    const start = Date.now();
    const container = await TestUtils.dbUp(TestUtils.getEnvProperties());
    await connectToDb();
    console.log("beforeAll: ", Date.now() - start);
    return container;
  }

  public static async afterAll(
    container: StartedDockerComposeEnvironment
  ): Promise<void> {
    const start = Date.now();
    await TestUtils.dbDown(container);
    console.log("afterAll: ", Date.now() - start);
  }

  public static beforeEach = async (context: ApiIntegrationTest) => {
    context.startTime = Date.now();
    const { url, schema } = TestUtils.setRandomDatabaseUrlSchema();
    context.schema = schema;

    await TestUtils.initDB(url);
    context.api = new Api(url);
    await context.api.app.ready();
    context.server = supertest(context.api.app.server);
    context.beforeTime = Date.now() - context.startTime;
  };

  public static afterEach = async (context: ApiIntegrationTest) => {
    const start = Date.now();
    // await TestUtils.dropDbSchema(context.schema);

    const after = Date.now() - start;
    const before = context.beforeTime;
    const totalTime = Date.now() - context.startTime;
    const totalTest = totalTime - after - before;
    console.log(
      `Test took: ${totalTest}ms before ${before}ms after ${after}ms total: ${totalTime}ms | name: ${context.task.name} | mode: ${context.task.mode} | concurrent: ${context.task.concurrent}`
    );
  };
}
