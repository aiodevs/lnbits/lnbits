import { FastifyInstance, FastifyRequest } from "fastify";
import { fastifyPlugin } from "fastify-plugin";
import {getPrisma} from "@util/prisma";
import { Unauthorized } from "@util/constants";
import SessionService from "@components/auth/session.service";

const sessionService = new SessionService();

export const authentication = fastifyPlugin(
  (fastify: FastifyInstance, _: unknown, done: () => void) => {
    const authPreHandler = async (request: FastifyRequest) => {
      try {
        const sessionId = sessionService.getSessionIdFromCookie(request);
        if (!sessionId) {
          throw Error();
        }
        const session = await sessionService.getSession(sessionId);
        if(!session){
          throw Error();
        }
        const user = await getPrisma().user.findUnique({
          where: {
            id: session.user_id,
          },
        });
        if (!user) {
          throw Error();
        }
        request.user = user;
        sessionService.updateSession(sessionId);
      } catch (error) {
        throw new Unauthorized();
      }
    };
    fastify.decorate("authenticateUser", authPreHandler);
    done();
  }
);
