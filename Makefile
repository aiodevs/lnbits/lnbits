.PHONY: build build-api build-lnbits build-ui setup dev up restart initdb down kill-ports pg4admin pg4admin-setup

dev:
	make down
	make docker-dev
	(cd api && pnpm run dev &) && (cd ui && pnpm run dev &)

api-dev:
	cd api && pnpm run dev

ui-dev:
	cd ui && pnpm run dev

docker-dev:
	docker compose -f docker-compose.backend.yml up -d

pg4admin:
	docker compose -f docker-compose.pgAdmin.yml up -d

up:
	docker compose up -d

restart:
	make down
	make up

down:
	make kill-ports
	docker compose down

initdb:
	@echo "Setting up PostgreSQL..."
	docker compose exec db psql -U postgres -c "ALTER USER postgres PASSWORD 'myPassword';"
	docker compose exec db psql -U postgres -t -c "SELECT 1 FROM pg_database WHERE datname='lnbits'" | grep -q 1 || docker-compose exec db psql -U postgres -c "CREATE DATABASE lnbits"

kill-ports:
ifeq ($(OS),Windows_NT)
	FOR /F "tokens=5" %%t IN ('netstat -ano ^| findstr ":[8080,4321,5000,3000]"') DO taskkill /F /PID %%t || true
else
	kill -9 $$(lsof -t -i:8080) || true
	kill -9 $$(lsof -t -i:4321) || true
	kill -9 $$(lsof -t -i:5000) || true
	kill -9 $$(lsof -t -i:3000) || true
endif

build:
	make build-api
	make build-lnbits
	make build-ui
	
build-lnbits:	
	cd lnbits && docker build -t lnbitsdocker/lnbits-legend .

build-api:
	cd api &&  docker build -t atitlan-api .

build-ui:
	cd ui && docker build -t atitlan-ui .

setup:
ifeq ($(OS),Windows_NT)
	copy lnbits\.env.example lnbits\.env
	copy .env.example .env
	@echo LNBITS_DATABASE_URL="postgres://postgres:myPassword@db:5432/lnbits" >> lnbits\.env
else
	cp lnbits/.env.example lnbits/.env
	cp .env.example .env
	@echo 'LNBITS_DATABASE_URL="postgres://postgres:myPassword@db:5432/lnbits"' >> lnbits/.env
endif
	make build
	make up
	make initdb
	make pg4admin-setup
	make pg4admin

pg4admin-setup:
ifeq ($(OS),Windows_NT)
	@echo "Defaulting to Windows OS"
	choco install gettext --yes
else
	@echo "Defaulting to Linux OS"
	@echo "Installing gettext for envsubst via package manager..."
	sudo apt update -y && sudo apt install gettext -y
endif
	@echo "Generating .pgpass file, if this is failing make a copy of the .pgpass.template and replace the values manually"
	envsubst < .pgpass.template > ./.pgpass
	chmod 0600 .pgpass

	@echo "Generating servers.json file, if this is failing make a copy of the servers.json.template and replace the values manually"
	envsubst < servers.json.template > ./servers.json
