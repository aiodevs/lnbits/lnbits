export interface Theme {
  backgroundColor: string;
  backgroundColorAlternate: string;

  font: string;
  fontSize: string;
  textColor: string;
  textColorAlternate: string;

  buttonColor: string;
  buttonTextColor: string;
}

export const defaultTheme: Theme = {
  backgroundColor: '#7EC4F8',
  backgroundColorAlternate: '#BFB48F',

  font: 'Roboto, Arial, sans-serif',
  fontSize: '16px',
  textColor: '#252627',
  textColorAlternate: '#22031F',

  buttonColor: '#FCDDF2',
  buttonTextColor: '#22031F',
};