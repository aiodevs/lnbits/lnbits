import { FastifyReply, FastifyRequest } from "fastify";
import { LoginUser } from "./auth.schema";
import AuthService from "./auth.service";
import SessionService from "@components/auth/session.service";
import { BadRequest, NotFound } from "@util/constants";

export default class AuthController {
  private authService: AuthService = new AuthService();;
  private sessionService: SessionService = new SessionService();
  
  public login = async (
    req: FastifyRequest<{ Body: LoginUser }>,
    reply: FastifyReply
  ) => {
    const user = await this.authService.loginUser(req.body);
    const session = await this.sessionService.createSession(user.id);
    this.sessionService.setSessionCookie(reply, session.id);

    return {
       message: "Login successful",
       user:{
        email: user.email 
       }
      };
  }

  public logout = async (
    req: FastifyRequest,
    reply: FastifyReply
  ) => {
    const sessionId : number | null = this.sessionService.getSessionIdFromCookie(req);
    if(sessionId === null){
      throw new NotFound("No session id found");
    }
 
      const promise = this.sessionService.deleteSession(sessionId);
      this.sessionService.deleteSessionCookie(reply);
  
      await promise;
      return {
        message: "Logout successful"
      }
 
  }
}
