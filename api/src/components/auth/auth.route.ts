import { FastifyInstance, RouteOptions } from "fastify";
import { CreateUserSchema } from "@components/user/user.schema";
import AuthController from "@components/auth/auth.controller";
import UserController from "@components/user/user.controller";
import { LoginUserSchema, LogoutSchema, ProtectedSchema } from "@components/auth/auth.schema";
import { Routes } from "@plugins/initializeRoutes";

export default class AuthRoute implements Routes{
  public path = "/auth";
  public userController = new UserController();
  public authController = new AuthController(); 

  public initializeRoutes(
    fastify: FastifyInstance,
    opts: RouteOptions,
    done: () => void
  ) {
    fastify.route({
      method: "post",
      url: `${this.path}/signup`,
      schema: CreateUserSchema,
      handler: this.userController.createUser,
    });

    fastify.route({
      method: "post",
      url: `${this.path}/login`,
      schema: LoginUserSchema,
      handler: this.authController.login,
    });

    fastify.route({
      method: "post",
      url: `${this.path}/logout`,
      schema: LogoutSchema,
      handler: this.authController.logout,
    });

    fastify.route({
      method: "get",
      url: `${this.path}/valid-session`,
      schema: ProtectedSchema,
      preHandler: fastify.authenticateUser,
      handler: async (req, reply) => {
        return {
          message: "User session is valid",
          user: req.user,
        };
      }
    });

    done();
  }
}
