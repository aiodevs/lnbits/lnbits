import type { APIContext, APIRoute } from "astro";
import { Config } from "@/utils/config";

export const POST: APIRoute = async (astro: APIContext) => {
  const form = await astro.request.json();
  const email = form.email;
  const password = form.password;
  if (!email || !password) {
    return new Response(
      JSON.stringify({
        message: "Missing required fields",
      }),
      { status: 400 }
    );
  }

  const path = Config.API_BASE_URL + "auth/signup";
  try{
  const response: Response = await fetch(path, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ email: email, password: password }),
  });

  const body = await response.json();
  const cookie = response.headers.get('set-cookie');

  if (response.ok && cookie) {
    return new Response(body.message,{
        status: 200,
        headers: {
            'set-cookie': cookie
        }
    });
  }
  
  return new Response(
    JSON.stringify({
      message: body.message,
    }),
    { status: response.status }
  );
  } 
  catch(error){
    console.log(error);
    return new Response(
      JSON.stringify({
        message: "Internal Server Error",
      }),
      { status: 500 }
    );
  }
};
