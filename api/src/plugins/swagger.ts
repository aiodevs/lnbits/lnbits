import { FastifyInstance } from "fastify";
import fastifySwagger, { FastifyDynamicSwaggerOptions } from "@fastify/swagger";
import fastifySwaggerUi, { FastifySwaggerUiOptions } from "@fastify/swagger-ui";
import { fastifyPlugin } from "fastify-plugin";

export const initSwagger = fastifyPlugin(
  (fastify: FastifyInstance, _: unknown, done: () => void) => {
    const opts: FastifyDynamicSwaggerOptions = {
      swagger: {
        info: {
          title: "Atitlan IO API",
          description: "Api documentation for Atitlan IO",
          version: "0.0.1",
        },
        tags: [
          { name: "auth", description: "Authentication end-points" },
          { name: "user", description: "User related end-points" },
          { name: "health", description: "Health related end-points" },
        ],
        consumes: ["application/json"],
        produces: ["application/json"],
        securityDefinitions: {
          cookieAuth: {
            type: "apiKey",
            name: "aio-session",
            in: "cookie",
          },
        },
        schemes: ["http"],
        security: [],
      },
    };

    fastify.register(fastifySwagger, opts);

    const uiOpts: FastifySwaggerUiOptions = {
      routePrefix: "/api-docs",
      staticCSP: true,
      transformStaticCSP: (header) => header,
      uiConfig: {
        docExpansion: "list",
        deepLinking: false,
      },
    };

    fastify.register(fastifySwaggerUi, uiOpts);
    done();
  }
);
