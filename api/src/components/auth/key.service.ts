import {getPrisma} from "@/util/prisma";
import { Key } from "@prisma/client";
import { hash } from "bcrypt";

export default class KeyService {
    private saltRounds = 10;

    public async hashPassword(password: string ): Promise<string>{
        return hash(password, this.saltRounds);
    }

    public async createKey(password: string, user_id: number ):Promise<Key>{
        const hashedPassword = await this.hashPassword(password);

        const key = getPrisma().key.create({
            data: {
                hashed_password: hashedPassword,
                user_id: user_id
            }
        })
        return key;
    }
}