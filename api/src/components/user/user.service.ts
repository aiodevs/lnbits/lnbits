import { CreateUser, GetUser } from '@components/user/user.schema';
import {getPrisma} from '@util/prisma';
import { Conflict, NotFound } from '@util/constants';
import { User } from '@prisma/client';
import KeyService from '@components/auth/key.service';
import SessionService from '@components/auth/session.service';

export type EnrichedUser = User & {
  auth_session: {
    id: number;
    user_id: number;
    active_expires: bigint;
    idle_expires: bigint;
  }[];
};

export default class UserService {

  private keyService: KeyService = new KeyService();
  private sessionService: SessionService = new SessionService();

  public async createUser(newUser: CreateUser) : Promise<EnrichedUser> {
    const checkUserExists = await getPrisma().user.findUnique({
      where: {
        email: newUser.email
      }
    });

    if (checkUserExists) {
      throw new Conflict('User already exists');
    }

    const hashedPassword = await this.keyService.hashPassword(newUser.password);
    const now = Date.now();
    const activeExpires = this.sessionService.activeExpiration(now);
    const idleExpires = this.sessionService.idleExpiration(now);

    const user = await getPrisma().user.create({
      data: {
        email: newUser.email,
        key: {
          create: {
            hashed_password: hashedPassword,
          },
        },
        auth_session: {
          create: {
            active_expires: activeExpires,
            idle_expires: idleExpires,
          },
        },  
      },
      include: {
        auth_session: true,
      },
    });

    return user;
  }

  public async getUser(getUserData: GetUser):Promise<User> {
    const user = await getPrisma().user.findUnique({
      where: {
        email: getUserData.email
      }
    });

    if (!user) {
      throw new NotFound('User not found');
    }

    return user;
  }
}