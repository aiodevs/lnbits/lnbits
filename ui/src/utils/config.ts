import { env } from "process";

export class Config{
    public static API_BASE_URL = env.API_BASE_URL || "http://localhost:3000/api/v1/";
    public static UI_BASE_URL = env.UI_BASE_URL || "http://localhost:4321/api/";
}
