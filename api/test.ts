import TestUtils from "./test/integration/util/test-util";

const environment = await TestUtils.dbUp({
    user: "user",
    password: "password",
    port: "1234",
    db: "test",
    host: "localhost"
});

const container = environment.getContainer("test-db_1");
const networkName = container.getNetworkNames()[0];
console.log(container.getIpAddress(networkName));
console.log(container.getFirstMappedPort());