import { FastifyInstance, FastifyReply, FastifyRequest, RouteOptions } from "fastify";
import { Routes } from "@plugins/initializeRoutes";
import {getPrisma} from "@/util/prisma";
import { HealthDB, HealthServer } from "./health.schema";
import { Unavailable } from "@/util/constants";

export default class HealthRoute implements Routes{
    public path = "/health";

  public initializeRoutes(
    fastify: FastifyInstance,
    opts: RouteOptions,
    done: () => void
  ) {
    fastify.route({
      method: "GET",
      url: `${this.path}/server`,
      schema: HealthServer,
      handler:async (req: FastifyRequest, reply: FastifyReply) => {
        reply.code(200).send({status: "OK"});
      }
    });

    fastify.route({
        method: "GET",
        url: `${this.path}/db`,
        schema: HealthDB,
        handler:async (req: FastifyRequest, reply: FastifyReply) => {
          try{
            await getPrisma().$queryRaw`SELECT 1`;
            reply.code(200).send({status: "OK"});
          }catch(err){
            console.log("Error:", err);
            reply.code(503).send({status: "Error"});
          }
        }
      });


    done();
  }
}
