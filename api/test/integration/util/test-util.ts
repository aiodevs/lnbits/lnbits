import { exec as node_exec } from "node:child_process";
import { promisify } from "node:util";
import { UUID, randomUUID } from "node:crypto";
import { getPrisma } from "@/util/prisma";
import {
  DockerComposeEnvironment,
  StartedDockerComposeEnvironment,
} from "testcontainers";

const exec = promisify(node_exec);

export interface schemaUrl {
  url: string;
  schema: UUID;
}

export interface envProps {
  user: string;
  password: string;
  db: string;
  port: string;
  host: string;
}

export default class TestUtils {

  public static async resetDBviaPrisma(): Promise<void> {
    await getPrisma().session.deleteMany();
    await getPrisma().key.deleteMany();
    await getPrisma().user.deleteMany();
  }

  public static getEnvProperties(): envProps {
    const user = process.env.API_POSTGRES_USER;
    const password = process.env.API_POSTGRES_PASSWORD;
    const db = process.env.API_POSTGRES_DB;
    const port = process.env.API_POSTGRES_PORT;
    const host = process.env.API_POSTGRES_HOST;
    if (!user || !password || !db || !port || !host) throw new Error("Missing env vars");
    return { user, password, db, port, host };
  }

  public static async dbUp({
    user,
    port,
    db,
    password,
  }: envProps): Promise<StartedDockerComposeEnvironment> {
    console.log("dbUp: ", port);
    const container = await new DockerComposeEnvironment("./", "./docker-compose.test.yml")
    .withEnvironment({
      "API_POSTGRES_USER": user,
      "API_POSTGRES_PASSWORD": password,
      "API_POSTGRES_DB": db,
      "API_POSTGRES_PORT": port,
    }).up();

    return container;
  }

  public static async dbDown(
    docker: StartedDockerComposeEnvironment
  ): Promise<void> {
    try {
      await docker.stop();
    } catch (error) {
      console.error("Error bringing down the database:", error);
    }
  }

  public static async initDB(databaseURL: string): Promise<void> {
    try {
      const command = `DATABASE_URL=${databaseURL} pnpm prisma migrate deploy --schema ./src/prisma/schema.prisma`
      const result = await exec(command);
      // console.log(result.stdout);
    } catch (error) {
      console.error("Exec error:", error);
    }
  }

  public static databaseURL({user, password, host, port, db }:envProps){
    return `postgresql://${user}:${password}@${host}:${port}/${db}?schema=public`;
  }

  public static setRandomDatabaseUrlSchema(databaseUrl?: string): schemaUrl {
    const schema = randomUUID();
    const dbUrl = databaseUrl ? databaseUrl : process.env.DATABASE_URL;
    if (!dbUrl) {
      throw new Error("Please provide a DATABASE_URL environment variable.");
    }

    const url = new URL(dbUrl);
    url.searchParams.set("schema", schema);
    
    return { url: url.toString(), schema: schema };
  }
}