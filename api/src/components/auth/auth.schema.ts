import { Static, Type } from "@fastify/type-provider-typebox";
import { FastifySchema } from "fastify";
import {
  ERROR400,
  ERROR401,
  ERROR404,
  ERROR409,
  ERROR500,
  responseProperty,
} from "@util/constants";

export type LoginUser = Static<typeof LoginUserBody>;

export const LoginUserBody = Type.Object({
  email: Type.String({
    format: "email",
    errorMessage: { format: "Invalid Email" },
  }),
  password: Type.String(),
});

export const LoginUserSchema: FastifySchema = {
  description: "Login user and create a session cookie",
  tags: ["auth"],
  body: LoginUserBody,
  response: {
    201: {
      description: "Successful login response",
      type: "object",
      properties: {
        ...responseProperty,
        user: { type: "object", properties: { email: { type: "string" } } },
      },
      headers: {
        "aio-session": {
          type: "sessionID",
          description: "session cookie",
        },
      },
    },
    400: ERROR400,
    401: ERROR401,
    500: ERROR500,
  },
};

export const LogoutSchema: FastifySchema = {
  description: "Logout by destroying session cookie and session in db",
  tags: ["auth"],
  response: {
    201: {
      description: "Successful login response",
      type: "object",
      properties: {
        ...responseProperty,
        user: { type: "object", properties: { email: { type: "string" } } },
      },
    },
    404: ERROR404,
    500: ERROR500,
  },
};

export const ProtectedSchema: FastifySchema = {
  description: "Protected route",
  tags: ["auth"],
  response: {
    200: {
      description: "Successful get response",
      type: "object",
      properties: {
        ...responseProperty,
        user: { type: "object", properties: { email: { type: "string" } } },
      },
    },
    401: ERROR401,
    404: ERROR404,
    500: ERROR500,
  },
  security: [
    {
      cookieAuth: []
    }
  ]
};
