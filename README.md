# Atitlan IO LNBTIS

## Setup Docker LNBits Image
### Install makefile, grep & Docker


For windows install choco and:
```
choco install make
choco install grep
```

For linux:
``` 
sudo apt update
sudo apt install build-essential 
```

### Create a docker image & setup
open a terminal and go to the lnbits main directory to run:
```
make build
```
To create a docker image

```
make setup-windows
make setup-linux
```
For windows/linux respectively to create a .env file and connect it to postgres
Feel free to edit the .env file

### Run the lnbits docker image and setup DB
```
make up
make initdb
```
To stop:
```
make down
```




