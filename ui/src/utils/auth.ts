import { Config } from "@/utils/config";
import type { AstroGlobal } from "astro";
import type { AstroComponentFactory } from "astro/runtime/server/index.js";

export const auth = async (Astro: Readonly<AstroGlobal<Record<string, any>, AstroComponentFactory, Record<string, string | undefined>>>):Promise<Response> => {
  const url = Config.UI_BASE_URL + "auth/valid-session";
  const cookie = Astro.request.headers.get("cookie");
  if (!cookie || !cookie.includes("session=")) {
    console.log("No session");
    return Astro.redirect("/login");
  }
  const result = await fetch(url, {
    method: "GET",
    headers: { cookie: cookie },
  });
  
  if(!result.ok){
    return Astro.redirect("/login")
  }

  return result;
};
