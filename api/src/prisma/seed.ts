import {getPrisma} from "@util/prisma";
import UserService from "@/components/user/user.service";
import KeyService from "@/components/auth/key.service";

console.log('Start.');
let count = await getPrisma().user.count();

main()
  .catch(e => {
    throw e
  })
  .finally(async () => {
    await getPrisma().$disconnect()
  });
  const userService = new UserService();
  const keyService = new KeyService();

async function main() {
  console.log('Seeding the database...');
  console.log(`There are ${await getPrisma().user.count()} users in the database and ${await getPrisma().key.count()} keys.`);

  try {
    const email = `john-${Math.random()}@example.com`;
    const password = 'password';

    const user = await userService.createUser({
      email: email,
      password: password
    });

    console.log(`Created user with email: ${user.email}`);
    console.log(`There are ${await getPrisma().user.count()} users in the database and ${await getPrisma().key.count()} keys.`);
  } catch (error) {
    if (error instanceof Error) {
      console.log(`Failed to create a user: ${error.message}`);
    } else {
      console.log(`Failed to create a user: ${error}`);
    }
    
  }
}