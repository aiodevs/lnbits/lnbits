import { FastifyPluginCallbackTypebox } from "@fastify/type-provider-typebox";
import { FastifyPluginOptions, FastifyInstance, RouteOptions } from "fastify";
import AuthRoute from "@components/auth/auth.route";
import HealthRoute from "@/components/health/health.route";

export interface Routes {
  path: string;
  initializeRoutes: (
    fastify: FastifyInstance,
    opts: RouteOptions,
    done: () => void
  ) => void;
}

export const initializeRoutes: FastifyPluginCallbackTypebox<
  FastifyPluginOptions
> = (server, options, done) => {
  // add the new routes here
  const routes = [
    new AuthRoute(),
    new HealthRoute()
  ];

  routes.forEach((route: Routes) => {
    server.register(route.initializeRoutes.bind(route));
  });
  done();
};
