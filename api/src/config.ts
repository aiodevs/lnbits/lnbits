import { Type } from "@sinclair/typebox";
import { config } from "dotenv";

loadConfigFile();
process.env.DATABASE_URL = generateDatabaseURL();

export const envSchema = Type.Object({
  NODE_ENV: Type.String(),
  API_VERSION: Type.String(),
  COOKIE_SECRET: Type.String(),
  // ORIGIN: Type.String(),
  // SECRET_KEY: Type.String(),
  // PORT: Type.String()
});

export const {
  NODE_ENV,
  PORT,
  API_VERSION,
  COOKIE_SECRET,
  // ORIGIN,
  // CREDENTIALS,
  // SECRET_KEY
} = process.env;

function loadConfigFile(){
  const envPath = `.env.${process.env.NODE_ENV ?? "development"}.local`;
  const result = config({ path: envPath });
  
  if (result.error) {
    console.log(`loading config from: ${envPath} failed`);
    throw result.error;
  }
}

function generateDatabaseURL(): string {
  const POSTGRES_USER = process.env.API_POSTGRES_USER!;
  const POSTGRES_PASSWORD = process.env.API_POSTGRES_PASSWORD!;
  const POSTGRES_DB = process.env.API_POSTGRES_DB!;
  const POSTGRES_HOST = process.env.API_POSTGRES_HOST!;
  const POSTGRES_PORT = process.env.API_POSTGRES_PORT!;

  const DATABASE_URL = `postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}?schema=public`;
  return DATABASE_URL;
}