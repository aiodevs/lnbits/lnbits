# api

A thin layer that acts as a proxy for the lnbits api.

## Initialize
Install pnpm (https://pnpm.io/installation)

Make sure the postgress server is running
```bash
make docker-dev
```
and then:
```bash
make init
```

To run:

```bash
make dev
```

To seed database run:

```bash
make seed
```