import {
    expect,
  test,
} from "vitest";
import TestUtils from "../util/test-util";
import supertest from "supertest";
import Api from "@/api";

test("Health server", async () => {
    const api = new Api();
    await api.app.ready();
    const request = supertest(api.app.server);

    const response = await request.get("/api/v1/health/server");

    expect(response.status).toBe(200);
    expect(response.body.status).toBe("OK");
});

test("Health db - down", async () => {
    const api = new Api("someStrangeDbURL");
    await api.app.ready();
    const request = supertest(api.app.server);

    const response = await request.get("/api/v1/health/db");

    expect(response.status).toBe(503);
    expect(response.body.status).toBe("Error");
});

test("Health db - up", async () => {
    const props = TestUtils.getEnvProperties();
    props.port = "5554";

    const environment = await TestUtils.dbUp(props);
    const url = TestUtils.databaseURL(props);
    const api = new Api(url);

    await TestUtils.initDB(url);
    await api.app.ready();
    const request = supertest(api.app.server);

    const response = await request.get("/api/v1/health/db");

    expect(response.status).toBe(200);
    expect(response.body.status).toBe("OK");
});