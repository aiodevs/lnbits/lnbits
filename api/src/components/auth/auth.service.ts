import { compare } from "bcrypt";
import { LoginUser } from "@components/auth/auth.schema";
import {getPrisma} from "@util/prisma";
import { NotFound, Unauthorized } from "@util/constants";
import { User } from "@prisma/client";

export default class AuthService {

  public async loginUser(loginData: LoginUser) : Promise<User> {
    const user = await getPrisma().user.findUnique({
      where: {
        email: loginData.email,
      },
    });

    if (!user) {
      console.log(`User not found ${loginData.email}`);
      throw new Unauthorized("User or password incorrect");
    }

    const key = await getPrisma().key.findFirst({
      where: {
        user_id: user.id,
      },
    });

    if (!key || key.hashed_password === null) {
      throw new Unauthorized("User or password incorrect");
    }

    const validPassword = await compare(loginData.password, key.hashed_password);
    if(!validPassword){
      throw new Unauthorized("User or password incorrect");
    }

    return user;
  }
}
