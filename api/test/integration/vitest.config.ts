import { defineConfig } from 'vitest/config';

export default defineConfig({
  resolve:{
      alias: {
        '@': './src',
        "@util": "./src/util",
        "@components": "./src/components",
        "@plugins": "./src/plugins",
      },
    },
    test:{
      testTimeout: 100000,
      hookTimeout: 100000,
      coverage: {
        reporter: ['text', 'json', 'html'],
        reportsDirectory: './test/integration/coverage'
      },
    },
});
