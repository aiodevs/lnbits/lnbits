import { FastifySchema } from "fastify";
import { ERROR503, responseProperty } from "@util/constants";

export const HealthServer: FastifySchema = {
  description: "Check the health of the server",
  tags: ["health"],
  response: {
    200: {
      description: "Successful login response",
      type: "object",
      properties: {
        ...responseProperty,
        status: { type: "string" },
      },
    },
    503: ERROR503,
  },
};
export const HealthDB: FastifySchema = {
  description: "Check the health of the db",
  tags: ["health"],
  response: {
    200: {
      description: "Successful login response",
      type: "object",
      properties: {
        ...responseProperty,
        status: { type: "string" },
      },
    },
    503: {
      description: "Service Unavailable",
      type: "object",
      properties: {
        ...responseProperty,
        status: { type: "string" },
      },
    },
  },
};
