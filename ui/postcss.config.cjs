module.exports = {
    plugins: [
        require('cssnano')(),
        require('autoprefixer')(),
        require('postcss-nested')(),
        require('postcss-property-lookup')(),
        require('postcss-advanced-variables')(),
        require("tailwindcss/nesting"),
        require("tailwindcss")("./tailwind.config.js"),
    ],
};