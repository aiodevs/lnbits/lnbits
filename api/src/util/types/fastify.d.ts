import { User } from "@prisma/client";
import { FastifyRequest, FastifyReply } from 'fastify';

declare module "fastify" {
  interface FastifyRequest {
    user?: User;
  }
  interface FastifyInstance {
    authenticateUser: (request: FastifyRequest, reply: FastifyReply) => Promise<void>;
  }
}