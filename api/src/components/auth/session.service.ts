import { BadRequest } from "@/util/constants";
import { Session } from "@prisma/client";
import {getPrisma} from "@util/prisma";
import { FastifyReply, FastifyRequest } from "fastify";

export const sessionCookieName = "aio-session";

export default class SessionService {
  // TODO move to env?
  private activeExpirationTime = 12 * 60 * 60 * 1000; // 3 hours
  private idleExpirationTime = 30 * 60 * 1000; // 30 min

  public activeExpiration(now: number) : number {
    return now + this.activeExpirationTime;
  }

  public idleExpiration(now: number) : number {
    return now + this.idleExpirationTime;
  }

  public async createSession(userId: number) : Promise<Session>{
    const now = Date.now();
    return getPrisma().session.create({
      data: {
        user_id: userId,
        active_expires: this.activeExpiration(now),
        idle_expires: this.idleExpiration(now),
      },
    });
  }

  public async getSession(sessionId: number) : Promise<Session | null>{
    return getPrisma().session.findUnique({
      where: { id: sessionId },
    });
  }

  public async updateSession(sessionId: number): Promise<Session> {
    const currentTime = Date.now();
    const idleExpires = currentTime + this.idleExpirationTime;

    return getPrisma().session.update({
      where: { id: sessionId },
      data: {
        idle_expires: idleExpires,
      },
    });
  }

  public async deleteSession(sessionId: number): Promise<Session> {
    const session = await this.getSession(sessionId);
    if(!session){
      throw new BadRequest("Session not found - trying to logout for a non-existant session");
    }

    return getPrisma().session.delete({
      where: { id: sessionId },
    });
  }

  public setSessionCookie(reply: FastifyReply, sessionId: number):void {
    reply.setCookie(sessionCookieName, sessionId.toString(), {
      path: "/",
      signed: true,
      httpOnly: true,
      sameSite: "strict",
      secure: process.env.NODE_ENV === "production",
    });
  }

  public deleteSessionCookie(reply: FastifyReply):void {
    reply.clearCookie(sessionCookieName);
  }

  public getSessionIdFromCookie(req: FastifyRequest): number | null {
    const sessionIdString = req.cookies[sessionCookieName];
    if (!sessionIdString) return null;

    const sessionId = parseInt(sessionIdString);
    return isNaN(sessionId) ? null : sessionId;
  }
}