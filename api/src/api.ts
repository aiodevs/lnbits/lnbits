import Fastify, { FastifyInstance } from "fastify";
import ajvErrors from "ajv-errors";
import fastifyHelmet from "@fastify/helmet";
import cookie from "@fastify/cookie";
import fastifyCompress from "@fastify/compress";
import { TypeBoxTypeProvider } from "@fastify/type-provider-typebox";
import { initializeRoutes } from "@plugins/initializeRoutes";
import { authentication } from "@plugins/authentication";
import { initSwagger } from "@plugins/swagger";
import { schemaErrorFormatter } from "@util/schemaErrorFormatter";
import { setPrismaDbUrl } from "./util/prisma";
import fastifyEnv from "@fastify/env";
import {
  API_VERSION,
  NODE_ENV,
  PORT,
  COOKIE_SECRET,
  envSchema as schema,
} from "@/config";


export default class Api {
  public app: FastifyInstance;
  public env: string;
  public port: number;

  constructor(databaseUrl?: string) {
    this.app = Fastify({
      schemaErrorFormatter,
      ajv: {
        customOptions: {
          coerceTypes: false,
          allErrors: true,
        },
        plugins: [ajvErrors],
      },
      logger: true,
    }).withTypeProvider<TypeBoxTypeProvider>();

    this.env = NODE_ENV ?? "development";
    this.port = Number(PORT) ?? 3001;
    this.init();
    if(databaseUrl)
      setPrismaDbUrl(databaseUrl);
  }

  public async listen() {
    try {
      await this.app.listen({ port: this.port, host: "0.0.0.0" });
    } catch (err) {
      this.app.log.error(err);
    }
  }

  private init(): void {
    this.initializePlugins();
    this.initializeRoutes();
    //this.initializeErrorHandling();
  }

  private initializePlugins(): void {
    this.app.register(fastifyEnv, { dotenv: true, schema });
    this.app.register(cookie, { secret: COOKIE_SECRET });
    // TODO CSRF protection
    this.app.register(fastifyHelmet);
    this.app.register(fastifyCompress);
    this.app.register(authentication);
    this.app.register(initSwagger);
  }

  private initializeRoutes() {
    this.app.register(initializeRoutes, { prefix: `api/${API_VERSION}` });
  }

  // private initializeErrorHandling() {
  //   this.app.setErrorHandler((error: FastifyError, request, reply) => {
  //     const status: number = error.statusCode ?? 500;
  //     const message: string =
  //       status === 500
  //         ? "Something went wrong"
  //         : error.message ?? "Something went wrong";

  //     this.app.log.error(
  //       `[${request.method}] ${request.url} >> StatusCode:: ${status}, Message:: ${message}`
  //     );

  //     return reply.status(status).send({ error: true, message });
  //   });
  // }
}
