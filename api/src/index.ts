import Api from '@/api';
import { connectToDb as connectDB } from './util/prisma';

const api = new Api();
api.listen();

connectDB()
.catch((e) => {
    throw e;
});