import {
  beforeAll,
  expect,
  test,
  afterEach,
  beforeEach,
} from "vitest"
import { ApiIntegrationTest, IntegrationTestFastResetDB, testSuite } from "../util/integration-test";

let port: string = "5555";
let suite: testSuite;

beforeAll(async () => {
  suite = await IntegrationTestFastResetDB.beforeAll(port);
});

beforeEach<ApiIntegrationTest>( async (context: ApiIntegrationTest) =>
{
  await IntegrationTestFastResetDB.beforeEach(context, suite);
});

afterEach<ApiIntegrationTest>(IntegrationTestFastResetDB.afterEach);

test<ApiIntegrationTest>("Auth - create user - 200 - ok", async (c) => {
  const response = await c.server.post("/api/v1/auth/signup").send({
    email: "test@test.com",
    password: "longasspassword",
  });

  expect(response.status).toBe(200);
  expect(response.body.user.email).toBe("test@test.com");
  expect(response.body.message).toBe("Signup successful");

  // Check for the session cookie
  const cookies = response.headers["set-cookie"];
  const aioSessionCookie = cookies?.find((cookie: string) =>
    cookie.startsWith("aio-session=")
  );

  expect(aioSessionCookie).toBeDefined();
  expect(aioSessionCookie).not.toBe("");
});

test<ApiIntegrationTest>("Auth - create user - 400 - short password, missing fields", async (c) => {
  let response = await c.server.post("/api/v1/auth/signup").send({
    email: "test@test.com",
    password: "toshort",
  });

  expect(response.status).toBe(400);
  expect(response.body.message).toBe(
    "password: Password must be at least 8 characters long"
  );

  response = await c.server.post("/api/v1/auth/signup").send({
    password: "goodlengthbaby",
  });

  expect(response.status).toBe(400);
  expect(response.body.message).toBe("must have required property 'email'");

  response = await c.server.post("/api/v1/auth/signup").send({
    email: "test@test.com",
  });

  expect(response.status).toBe(400);
  expect(response.body.message).toBe("must have required property 'password'");
});

test<ApiIntegrationTest>("Auth - create user - 409 - user exists", async (c) => {
  await c.server.post("/api/v1/auth/signup").send({
    email: "test@test.com",
    password: "password",
  });
  const response = await c.server.post("/api/v1/auth/signup").send({
    email: "test@test.com",
    password: "password",
  });

  expect(response.status).toBe(409);
  expect(response.body.message).toBe("User already exists");
});

test<ApiIntegrationTest>("Auth - login - 200 - ok", async (c) => {
  await c.server.post("/api/v1/auth/signup").send({
    email: "test@test.com",
    password: "password",
  });

  const response = await c.server.post("/api/v1/auth/login").send({
    email: "test@test.com",
    password: "password",
  });

  expect(response.status).toBe(200);
  expect(response.body.message).toBe("Login successful");
  expect(response.body.user.email).toBe("test@test.com");

  // Check for the session cookie
  const cookies = response.headers["set-cookie"];
  const aioSessionCookie = cookies?.find((cookie: string) =>
    cookie.startsWith("aio-session=")
  );

  expect(aioSessionCookie).toBeDefined();
  expect(aioSessionCookie).not.toBe("");
});

test<ApiIntegrationTest>("Auth - login - 401/404 - wrong login", async (c) => {
  await c.server.post("/api/v1/auth/signup").send({
    email: "test@test.com",
    password: "password",
  });

  // Wrong password
  let response = await c.server.post("/api/v1/auth/login").send({
    email: "test@test.com",
    password: "wrongpassword",
  });

  expect(response.status).toBe(401);
  expect(response.body.message).toBe("User or password incorrect");

  // Check for the session cookie
  let cookies = response.headers["set-cookie"];
  let aioSessionCookie = cookies?.find((cookie: string) =>
    cookie.startsWith("aio-session=")
  );

  expect(aioSessionCookie).toBeUndefined();

  // User wrong
  response = await c.server.post("/api/v1/auth/login").send({
    email: "wronguser@test.com",
    password: "password",
  });

  expect(response.status).toBe(401);
  expect(response.body.message).toBe("User or password incorrect");

  // Check for the session cookie
  cookies = response.headers["set-cookie"];
  aioSessionCookie = cookies?.find((cookie: string) =>
    cookie.startsWith("aio-session=")
  );

  expect(aioSessionCookie).toBeUndefined();
});

test<ApiIntegrationTest>("Auth - valid-session - 200 ok", async (c) => {
  let signupResponse = await c.server.post("/api/v1/auth/signup").send({
    email: "user@example.com",
    password: "securepassword",
  });

  const cookies = signupResponse.headers["set-cookie"];

  const protectedResponse = await c.server
    .get("/api/v1/auth/valid-session")
    .set("Cookie", cookies)
    .send();

  expect(protectedResponse.status).toBe(200);
  expect(protectedResponse.body.message).toBe("User session is valid");
  expect(protectedResponse.body.user.email).toBe("user@example.com");
});

test<ApiIntegrationTest>("Auth - valid-session - 401 - without cookie ", async (c) => {
  const response = await c.server.get("/api/v1/auth/valid-session").send();

  expect(response.status).toBe(401);
  expect(response.body.message).toBe("Unauthorized");
});

test<ApiIntegrationTest>("Auth - Logout - 200 ok", async (c) => {
  let signupResponse = await c.server.post("/api/v1/auth/signup").send({
    email: "user@example.com",
    password: "securepassword",
  });

  const cookies = signupResponse.headers["set-cookie"];

  const logoutResponse = await c.server
    .post("/api/v1/auth/logout")
    .set("Cookie", cookies)
    .send();

  expect(logoutResponse.status).toBe(200);
  expect(logoutResponse.body.message).toBe("Logout successful");
});

test<ApiIntegrationTest>("Auth - Logout - 400 double logout", async (c) => {
  let signupResponse = await c.server.post("/api/v1/auth/signup").send({
    email: "user@example.com",
    password: "securepassword",
  });

  const cookies = signupResponse.headers["set-cookie"];

  await c.server.post("/api/v1/auth/logout").set("Cookie", cookies).send();

  const logoutResponse = await c.server
    .post("/api/v1/auth/logout")
    .set("Cookie", cookies)
    .send();

  expect(logoutResponse.status).toBe(400);
  expect(logoutResponse.body.message).toBe(
    "Session not found - trying to logout for a non-existant session"
  );
});

test<ApiIntegrationTest>("Auth - Logout - 404 - without cookie", async (c) => {
  const response = await c.server.post("/api/v1/auth/logout").send();

  expect(response.status).toBe(404);
  expect(response.body.message).toBe("No session id found");
});
